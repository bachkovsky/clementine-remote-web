name := """ClementineRemoteApi"""
organization := "bachkovsky"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.12.6"
scalacOptions += "-Ypartial-unification"
libraryDependencies += guice
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.2" % Test

libraryDependencies += "org.typelevel" %% "cats-core" % "1.3.1"
libraryDependencies += "org.typelevel" %% "cats-effect" % "1.0.0"
libraryDependencies += "com.softwaremill.common" %% "tagging" % "2.2.1"