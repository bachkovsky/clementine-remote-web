import com.google.inject.AbstractModule
import model.{Clementine, ClementineLinuxController}

class Module extends AbstractModule {

  override def configure() = {
    bind(classOf[Clementine]).to(classOf[ClementineLinuxController])
  }

}
