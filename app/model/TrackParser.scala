package model

import scala.util.parsing.combinator.JavaTokenParsers

class TrackParser extends JavaTokenParsers {

  def meta(key: String): Parser[Option[String]] =
    opt(key ~> """(.*?)\n""".r ^^ { _.toString })
}

object TrackParser extends TrackParser {
  def parse(trackRaw: String) = ???


  def main(args: Array[String]): Unit = {
    val unit = parseAll(meta("sos: "), "sos: asd\n")
    println(unit)
  }
}
