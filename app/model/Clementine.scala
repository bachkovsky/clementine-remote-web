package model

import cats.effect.IO
import model.TrackParser.ParseResult

import scala.language.postfixOps

trait Clementine {
  def playNext(): IO[Unit]

  def playPrev(): IO[Unit]

  def playPause(): IO[Unit]

  def volumeUp(amount: Int = 10): IO[Unit]

  def volumeDown(amount: Int = 10): IO[Unit]

  def like(): IO[Unit]

  def currentTrack: IO[ParseResult[Track]]
}


