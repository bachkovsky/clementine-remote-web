package model

import cats.effect.IO
import javax.inject.Singleton

import scala.language.postfixOps
import scala.sys.process._
import model.TrackParser.ParseResult

@Singleton
class ClementineLinuxController extends Clementine {
  override def playNext(): IO[Unit] =
    IO(
      "qdbus org.mpris.MediaPlayer2.clementine /Player org.freedesktop.MediaPlayer.Next" !)

  override def playPrev(): IO[Unit] =
    IO(
      "qdbus org.mpris.MediaPlayer2.clementine /Player org.freedesktop.MediaPlayer.Prev" !)

  override def volumeUp(amount: Int): IO[Unit] =
    IO(
      s"qdbus org.mpris.MediaPlayer2.clementine /Player org.freedesktop.MediaPlayer.VolumeUp $amount" !)

  override def volumeDown(amount: Int): IO[Unit] =
    IO(
      s"qdbus org.mpris.MediaPlayer2.clementine /Player org.freedesktop.MediaPlayer.VolumeDown $amount" !)

  override def currentTrack: IO[ParseResult[Track]] =
    for {
      trackRaw <- currentTrackRaw
    } yield TrackParser.parse(trackRaw)

  private def currentTrackRaw = {
    IO(
      "qdbus org.mpris.MediaPlayer2.clementine /Player org.freedesktop.MediaPlayer.GetMetadata" !!)
  }

  override def like(): IO[Unit] =
    //there is no mpris function to do that, so use pre-configured hotkey
    IO("xdotool key alt+Insert" !)

  override def playPause(): IO[Unit] =
    IO(
      "qdbus org.mpris.MediaPlayer2.clementine /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.PlayPause" !)
}
