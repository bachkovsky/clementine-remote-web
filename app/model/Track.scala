package model

import com.softwaremill.tagging.@@
import model.Track.{Tag, _}
import play.api.libs.json._

case class Track(album: Tag @@ Album,
                 artist: Tag @@ Artist,
                 artUrl: Tag @@ ArtUrl,
                 audioBitRate: Tag @@ AudioBitRate,
                 audioSampleRate: Tag @@ AudioSampleRate,
                 bpm: Tag @@ Bpm,
                 comment: Tag @@ Comment,
                 genre: Tag @@ Genre,
                 location: Tag @@ Location,
                 mTime: Tag @@ MTime,
                 time: Tag @@ Time,
                 title: Tag @@ Title,
                 trackNumber: Tag @@ TrackNumber,
                 year: Tag @@ Year)

object Track {

  type Tag = Option[String]

  trait Album

  trait Artist

  trait ArtUrl

  trait AudioBitRate

  trait AudioSampleRate

  trait Bpm

  trait Comment

  trait Genre

  trait Location

  trait MTime

  trait Time

  trait Title

  trait TrackNumber

  trait Year

  implicit val trackJsonWriter: Writes[Track] = (o: Track) =>
    Json.obj(
      "title" -> o.title,
      "artist" -> o.artist,
      "album" -> o.album,
      "year" -> o.year,
      "genre" -> o.genre
  )

}
