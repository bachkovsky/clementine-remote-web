package controllers

import javax.inject._
import model.{Clementine, Track}
import play.api.mvc._

@Singleton
class ClementineHtmlController @Inject()(cc: ControllerComponents,
                                         clementine: Clementine)
    extends AbstractClementineController(cc, clementine) {

  override protected def processCommand = { _ =>
    Redirect(routes.ClementineHtmlController.current())
  }

  override protected def processTrack(t: Track) = {
    Ok(views.html.index(t))
  }
}
