package controllers

import cats.effect.IO
import cats.implicits._
import model.TrackParser.{Failure, Success}
import model.{Clementine, Track}
import play.api.mvc.{AbstractController, ControllerComponents, Result}

abstract class AbstractClementineController(cc: ControllerComponents,
                                            clementine: Clementine)
    extends AbstractController(cc) {

  protected def processTrack(t: Track): Result

  protected def processCommand: Unit => Result = _ => Ok

  def current() = Action.async { _ =>
    val result: IO[Result] = for {
      maybeTrack <- clementine.currentTrack
      response <- maybeTrack match {
        case Success(t: Track, _) => IO(processTrack(t))
        case Failure(msg, _) =>
          IO(println(msg)) >> IO(InternalServerError("Track parsing error"))
      }
    } yield response

    result.unsafeToFuture()
  }

  def playNext() = asyncCmd(_.playNext())

  def playPrev() = asyncCmd(_.playPrev())

  def volumeUp() = asyncCmd(_.volumeUp())

  def volumeDown() = asyncCmd(_.volumeDown())

  def like() = asyncCmd(_.like())

  def playPause() = asyncCmd(_.playPause())

  private def asyncCmd(io: Clementine => IO[Unit]) = Action.async { _ =>
    io(clementine).map(processCommand).unsafeToFuture()
  }
}
