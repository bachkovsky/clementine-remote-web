package controllers

import javax.inject.{Inject, Singleton}
import model.{Clementine, Track}
import play.api.libs.json.Json
import play.api.mvc.ControllerComponents

@Singleton
class ClementineRestController @Inject()(cc: ControllerComponents,
                                         clementine: Clementine)
    extends AbstractClementineController(cc, clementine) {

  override protected def processTrack(t: Track) = Ok(Json.toJson(t))
}
